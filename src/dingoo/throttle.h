#ifndef THROTTLE_H
#define THROTTLE_H

#include <stdlib.h>
#include <stdio.h>

#include "lyngoo.h"

#include "System.h"
#include "Lynxdef.h"

extern volatile ULONG mEmulationSpeed;
extern volatile ULONG mFrameCount;
extern volatile ULONG mFramesPerSecond;
extern ULONG mFrameSkip;

bool ThrottleInit();
void ThrottleExit();
bool ThrottleCheck();

#endif
