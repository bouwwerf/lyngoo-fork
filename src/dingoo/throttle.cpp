#include "throttle.h"

#define  THROTTLE_TASK_START_PRIO 10                        // Must be unique!
#define  THROTTLE_TASK_STK_SIZE   1024                      // Size of each task's stacks (# of WORDs)

#define  THROTTLE_TIME   TIMER_RESOLUTION/HANDY_TIMER_FREQ

OS_STK   ThrottleTaskStartStk[THROTTLE_TASK_STK_SIZE];

volatile bool throttle_thread_active = false;
volatile ULONG mEmulationSpeed = 100;
volatile ULONG mFrameCount = 0;
volatile ULONG mFramesPerSecond = 0;
ULONG mFrameSkip = 0;
timer *throttle_timer;

// *************************************************
static void ThrottleThread(void *none)
{
   static ULONG count=0;
   static ULONG fcount=0;
   static ULONG last_cycle_count=0;
   static ULONG last_frame_count=0;

   ULONG throttle_timer_last=timer_total(throttle_timer), throttle_timer_now;

   throttle_thread_active=true;

   do {
      throttle_timer_now=timer_total(throttle_timer);
      if ((throttle_timer_now-throttle_timer_last) >= THROTTLE_TIME) {
         throttle_timer_last=throttle_timer_now;

         // Calculate emulation speed
         count++;
         fcount++;

         if(count==HANDY_TIMER_FREQ/2)
         {
            count=0;
            if(last_cycle_count>gSystemCycleCount)
            {
               mEmulationSpeed=0;
            }
            else
            {
               // Add .5% correction factor for round down error
               mEmulationSpeed=(gSystemCycleCount-last_cycle_count)*100;
               mEmulationSpeed+=(gSystemCycleCount-last_cycle_count)/2;
               mEmulationSpeed/=HANDY_SYSTEM_FREQ/2;
             }
             last_cycle_count=gSystemCycleCount;
         }

         if(fcount==HANDY_TIMER_FREQ)
         {
            fcount=0;
            if(last_frame_count>mFrameCount)
            {
               mFramesPerSecond=0;
            }
            else
            {
               mFramesPerSecond=(mFrameCount-last_frame_count);
            }
            last_frame_count=mFrameCount;
         }

         // Increment system counter
         gTimerCount++;

      } else OSTimeDly(1);
   }
   while (throttle_thread_active);
   throttle_thread_active=true;
   OSTaskDel(THROTTLE_TASK_START_PRIO);
}

// *************************************************
bool ThrottleInit()
{
   throttle_timer=timer_create();
   throttle_thread_active=false;
   return (OSTaskCreate(ThrottleThread, NULL, &ThrottleTaskStartStk[THROTTLE_TASK_STK_SIZE - 1], THROTTLE_TASK_START_PRIO) == 0);
}

// *************************************************
void ThrottleExit()
{
   if (throttle_thread_active) {
      throttle_thread_active=false;
      while (!throttle_thread_active);
   }
   timer_delete(throttle_timer);
}

// *************************************************
bool ThrottleCheck()
{
   if(gSystemCycleCount>gThrottleNextCycleCheckpoint)
   {
      static int limiter=0;
      static int flipflop=0;
      int overrun=gSystemCycleCount-gThrottleNextCycleCheckpoint;
      int nextstep=(((HANDY_SYSTEM_FREQ/HANDY_TIMER_FREQ)*gThrottleMaxPercentage)/100);

      // We've gone thru the checkpoint, so therefore we
      // must have reached the next timer tick, if the
      // timer hasnt ticked then we've got here early.
      if(gThrottleLastTimerCount==gTimerCount)
      {
         // All we know is that we got here earlier than expected as the
         // counter has not yet rolled over
         if(limiter<0) limiter=0; else limiter++;
         if(limiter>40 && mFrameSkip>0)
         {
//            mFrameSkip--;
            limiter=0;
         }
         flipflop=1;
         return false;
      }

      // Frame Skip adjustment
      if(!flipflop)
      {
         if(limiter>0) limiter=0; else limiter--;
         if(limiter<-7 && mFrameSkip<10)
         {
//            mFrameSkip++;
            limiter=0;
         }
      }

      flipflop=0;

      // Set the next control point
      gThrottleNextCycleCheckpoint+=nextstep;

      // Set next timer checkpoint
      gThrottleLastTimerCount=gTimerCount;

      // Check if we've overstepped the speed limit
      if(overrun>nextstep)
      {
         // We've exceeded the next timepoint, going way too
         // fast (sprite drawing)
         return false;
      }
   }

   return true;
}

