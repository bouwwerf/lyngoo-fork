#ifndef LYNGOO_H
#define LYNGOO_H

#include <stdlib.h>
#include <stdio.h>

#include <dingkit/audio.h>
#include <dingkit/clock.h>
#include <dingkit/dingoo.h>
#include <dingkit/timer.h>
#include <dingkit/ucos2.h>

#include "System.h"
#include "Lynxdef.h"

#include "dingoogfx.h"
#include "pad.h"
#include "sound.h"
#include "throttle.h"

#define FILE_VERSION		"LynGOO v1.4"
#define BOUWWERF_INFO		"Updated by BouwWerf"

#define ACTIONS_VIEW		0x2000000
#define VIEW_REQUEST		0x1000000
#define ACTION_CONTINUE		1
#define ACTION_LOAD_STATE	2
#define ACTION_SAVE_STATE	3
#define ACTION_RESET		4
#define ACTION_QUIT		5
#define OPTION_VOLUME		6
#define OPTION_SOUND		7
#define OPTION_SCREENMODE	8
#define OPTION_SCREENROTATE	9
#define OPTION_LYNXSPEED	10
#define OPTION_SYSTEMCLOCK	11

#define ROM_FILE		"A:\\GAME\\lynxboot.img"
#define STA_EXT			".sta"
#define MAXPATH			256

#define SCREENMODE_1X		1
#define SCREENMODE_2X		2
#define SCREENMODE_STRETCH	3

typedef struct
{
   char selectedRomName[MAXPATH];
   int loopCount, lynxSpeed, systemClock;
   int screenMode, screenRotate;
   int sndEnabled, sndLevel;
   int lynxKeyUp, lynxKeyDown, lynxKeyLeft, lynxKeyRight, lynxKeyA, lynxKeyB, lynxKeyOPT1, lynxKeyOPT2, lynxKeyStart;
   CSystem *lynxSystem;
} LynGOOConf;

extern LynGOOConf config;

uint32_t ActionSelectView();
int GetKeyIndex(int key);
void LoadState();
void SaveState();

#endif
