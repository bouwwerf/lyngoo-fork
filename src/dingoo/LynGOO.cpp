#include "lyngoo.h"

LynGOOConf config;
bool keepRunning = true;
int oldClock;
int screenWidth, screenHeight;


// **********************************************************
void LynGOODebug()
{
   char str[100];
   sprintf(str,"%d%% %d %d",mEmulationSpeed,mFramesPerSecond,mFrameSkip);
   PgPrint(0,0,RED,str,0);
}

// **********************************************************
UBYTE* LynGOODisplayCallback(ULONG objref) {
   static int frameskipcount = 0;
   UBYTE* VramAddr;

   PgSetVisible(screenWidth,screenHeight);

   if (config.screenRotate==MIKIE_NO_ROTATE) {
      VramAddr = (UBYTE*)PgGetVramAddr((screenWidth-HANDY_SCREEN_WIDTH)/2,(screenHeight-HANDY_SCREEN_HEIGHT)/2);
   } else {
      VramAddr = (UBYTE*)PgGetVramAddr((screenWidth-HANDY_SCREEN_HEIGHT)/2,(screenHeight-HANDY_SCREEN_WIDTH)/2);
   }

   if (frameskipcount>0) {
      frameskipcount--;
   } else {
      frameskipcount=mFrameSkip;
      mFrameCount++;
//      LynGOODebug();
      PgScreenFlip();
      ClearDoubleBuffer();
   }

   PgSetVisible(DINGOO_SCREEN_WIDTH,DINGOO_SCREEN_HEIGHT);

   return VramAddr;
}

// **********************************************************
void LynGOOSetup()
{
   // set system clock
   if (config.systemClock>0) {
      sys_pll_init(config.systemClock*1000*1000);
   } else {
      sys_pll_init(oldClock);
   }

   // determine screen width and height
   switch (config.screenMode) {
      case SCREENMODE_1X:
         screenWidth=DINGOO_SCREEN_WIDTH;
         screenHeight=DINGOO_SCREEN_HEIGHT;
      break;
      case SCREENMODE_2X:
         screenWidth=HANDY_SCREEN_WIDTH;
         screenHeight=DINGOO_SCREEN_HEIGHT/2;
      break;
      case SCREENMODE_STRETCH:
      default:
         screenWidth=HANDY_SCREEN_WIDTH;
         screenHeight=HANDY_SCREEN_HEIGHT;
      break;
   }

   // determine rotation
   if (config.screenRotate!=MIKIE_NO_ROTATE) {
      int tmp=screenWidth;
      screenWidth=screenHeight;
      screenHeight=tmp;
   }

   // correct for 4:3 screen ratio if neccessary
   if (config.screenRotate!=MIKIE_NO_ROTATE && config.screenMode!=SCREENMODE_STRETCH) {
      screenWidth=(screenWidth<<4)/9;
   }

   // resize if neccessary
   while (screenWidth<HANDY_SCREEN_WIDTH || screenHeight<HANDY_SCREEN_HEIGHT) {
       screenWidth<<=1;
       screenHeight<<=1;
   }
   while (screenWidth>DINGOO_SCREEN_WIDTH || screenHeight>DINGOO_SCREEN_HEIGHT) {
       screenWidth>>=1;
       screenHeight>>=1;
   }

   // set lynx display attributes
   config.lynxSystem->DisplaySetAttributes(config.screenRotate,MIKIE_PIXEL_FORMAT_16BPP_565,screenWidth*sizeof(uint16_t),LynGOODisplayCallback,NULL);

   // determine d-pad controls
   switch (config.screenRotate) {
      case MIKIE_NO_ROTATE:
         config.lynxKeyUp = DK_DPAD_UP;
         config.lynxKeyDown = DK_DPAD_DOWN;
         config.lynxKeyLeft = DK_DPAD_LEFT;
         config.lynxKeyRight = DK_DPAD_RIGHT;
      break;
      case MIKIE_ROTATE_L:			// somehow these are switched
         config.lynxKeyUp = DK_DPAD_RIGHT;
         config.lynxKeyDown = DK_DPAD_LEFT;
         config.lynxKeyLeft = DK_DPAD_UP;
         config.lynxKeyRight = DK_DPAD_DOWN;
      break;
      case MIKIE_ROTATE_R:			// L = rotated to the right, R = rotated to the left
      default:
         config.lynxKeyUp = DK_DPAD_LEFT;
         config.lynxKeyDown = DK_DPAD_RIGHT;
         config.lynxKeyLeft = DK_DPAD_DOWN;
         config.lynxKeyRight = DK_DPAD_UP;
      break;
   }

   // enable/disable sound
   gAudioEnabled = config.sndEnabled;

   // set lynx speed
   gThrottleMaxPercentage = config.lynxSpeed;
}

// **********************************************************
void LynGOOInit(int argc, char *argv[])
{
   // save system clock
   oldClock=detect_clock(1);

   // initialize pad
   PadInit();

   // initialize graphics
   PgInit();
   ClearDoubleBuffer();
   PgScreenFlip();

   // initialize config
   config.loopCount = 1000;
   config.lynxSpeed = 100;
   config.systemClock = 0;
   config.screenMode = SCREENMODE_2X;
   config.screenRotate = MIKIE_NO_ROTATE;
   config.sndEnabled = true;
   config.sndLevel = 30;
   config.lynxSystem = NULL;
   memset(config.selectedRomName, 0, MAXPATH);
   config.lynxKeyA = DK_BUTTON_A;
   config.lynxKeyB = DK_BUTTON_B;
   config.lynxKeyOPT1 = DK_BUTTON_X;
   config.lynxKeyOPT2 = DK_BUTTON_Y;
   config.lynxKeyStart = DK_BUTTON_START;

   // initialize selected rom name
   if (argc>0) strcpy(config.selectedRomName,argv[0]);

   // initialize lynx
   config.lynxSystem = new CSystem(config.selectedRomName, ROM_FILE);
   if (config.lynxSystem == NULL) {
      Error("Error creating lynx",1);
      keepRunning = false;
   } else {
      LynGOOSetup();
   }

   // initialize sound thread
   if (!SoundInit()) {
      Error("Error initializing sound thread",1);
      keepRunning = false;
   }

   // initialize throttle thread
   if (!ThrottleInit()) {
      Error("Error initializing throttle thread",1);
      keepRunning = false;
   }
}

// **********************************************************
void LynGOOExit()
{
   // denitialize throttle thread
   ThrottleExit();

   // deinitialize sound thread
   SoundExit();

   // deinitialize lynx
   delete config.lynxSystem;

   // deinitialize graphics
   PgExit();

   // deinitialize pad
   PadExit();

   // restore system clock
   sys_pll_init(oldClock);
}

// **********************************************************
uint32_t LynGOOMenu(uint32_t menu)
{
   uint32_t result;

   switch (menu)
   {
   case ACTIONS_VIEW:
   default:
      result = ActionSelectView();
      break;
   }

   if (result & VIEW_REQUEST)
   {
      LynGOOMenu(result ^= VIEW_REQUEST);
   }
   else if (result & (ACTIONS_VIEW))
   {
      uint32_t action = (result ^= ACTIONS_VIEW);

      switch (action)
      {
      case ACTION_CONTINUE:
         break;
      case ACTION_LOAD_STATE:
         if (Question("Load state?")==DK_BUTTON_A) 
           LoadState();
         break;
      case ACTION_SAVE_STATE:
         if (Question("Save state?")==DK_BUTTON_A) 
           SaveState();
         break;
      case ACTION_RESET:
         if (config.lynxSystem != NULL)
            config.lynxSystem->Reset();
         break;
      case ACTION_QUIT:
         keepRunning = false;
         break;
      }

      ClearDoubleBuffer();
   }
   return 0;
}

// **********************************************************
void SaveState()
{
   char contextName[MAXPATH];
   char romName[MAXPATH];

   if (config.lynxSystem != NULL)
   {
      strncpy(romName, config.selectedRomName, strlen(config.selectedRomName) - 4);
      romName[strlen(config.selectedRomName) - 4] = '\0';
      sprintf(contextName, "%s%s", romName, STA_EXT);
      if (!(config.lynxSystem->ContextSave(contextName)))
         Error("Error saving state.", 1);
   }
}

// **********************************************************
void LoadState()
{
   char contextName[MAXPATH];
   char romName[MAXPATH];

   if (config.lynxSystem != NULL)
   {
      strncpy(romName, config.selectedRomName, strlen(config.selectedRomName) - 4);
      romName[strlen(config.selectedRomName) - 4] = '\0';
      sprintf(contextName, "%s%s", romName, STA_EXT);
      if (!(config.lynxSystem->ContextLoad(contextName)))
         Error("Error loading state.", 1);
   }
}

// **********************************************************
int GetKeyIndex(int key)
{
   switch (key)
   {
   case DK_DPAD_UP:
      return 0;
   case DK_DPAD_DOWN:
      return 1;
   case DK_DPAD_LEFT:
      return 2;
   case DK_DPAD_RIGHT:
      return 3;
   case DK_BUTTON_A:
      return 4;
   case DK_BUTTON_B:
      return 5;
   case DK_BUTTON_X:
      return 6;
   case DK_BUTTON_Y:
      return 7;
   case DK_BUTTON_START:
      return 8;
   case DK_BUTTON_SELECT:
      return 9;
   case DK_TRIGGER_RIGHT:
      return 10;
   case DK_TRIGGER_LEFT:
      return 11;
   }

   return 0;
}

// **********************************************************
void MakeActionListView(const char **actionList, int numItems, int cursor)
{
   char buf[15];
   unsigned long color;

   ClearDoubleBuffer();
   PgPrint(15, 1, YELLOW, FILE_VERSION, 0);
   PgPrint(11, 2, YELLOW, BOUWWERF_INFO, 0);
   for (int i = 0, j = 7; i < numItems; i++, j++)
   {
      if (i == ACTION_QUIT) j++;
      if (i == cursor) color=RED; else color=WHITE;
      PgPrint(2, j, color, actionList[i], 0);
      switch(i+1) {
         case OPTION_VOLUME:
            sprintf(buf, "%d%%", config.sndLevel);
            break;
         case OPTION_SOUND:
            sprintf(buf, (config.sndEnabled ? "On" : "Off"));
            break;
         case OPTION_SCREENMODE:
            switch (config.screenMode) {
               case SCREENMODE_1X:
                  sprintf(buf, "Original");
               break;
               case SCREENMODE_2X:
                  sprintf(buf, "Zoomed");
               break;
               case SCREENMODE_STRETCH:
               default:
                  sprintf(buf, "Fullscreen");
               break;
            }
            break;
         case OPTION_SCREENROTATE:
            switch (config.screenRotate) {
               case MIKIE_NO_ROTATE:
                  sprintf(buf, "Off");
               break;
               case MIKIE_ROTATE_L:
                  sprintf(buf, "Right");	// somehow these are switched
               break;
               case MIKIE_ROTATE_R:
               default:
                  sprintf(buf, "Left");		// L = rotated to the right, R = rotated to the left
               break;
            }
            break;
         case OPTION_LYNXSPEED:
            sprintf(buf, "%d%%", config.lynxSpeed);
            break;
         case OPTION_SYSTEMCLOCK:
            if (config.systemClock == 0) {
               sprintf(buf, "Default");
            } else {
               sprintf(buf, "%d", config.systemClock);
            }
            break;
         default:
            sprintf(buf,"");
            break;
      }
      PgPrint(14, j, color, buf, 0);
   }
   PgScreenFlip();
}

// **********************************************************
uint32_t ActionSelectView()
{
   int cursor = 0;
   const char* actionList[11] = { "Continue", "Load state", "Save state", "Reset", "Quit", "Volume", "Sound", "Scaling", "Rotation", "Lynx speed", "System MHz" };
   int numItems = sizeof(actionList)/sizeof(actionList[0]);
   char selected = -1;
   char action = 0;

   while (selected < 0 && action < 2)
   {
      MakeActionListView(actionList, numItems, cursor);

      action = 0;

      while (action == 0)
      {
         PadKeyTimer();
         switch (PadWaitForOneKey())
         {
         case DK_DPAD_UP:
            cursor--;
            action = 1;
            if (cursor < 0)
               cursor = (numItems-1);
            break;
         case DK_DPAD_DOWN:
            cursor++;
            action = 1;
            if (cursor >= numItems)
               cursor = 0;
            break;
         case DK_DPAD_LEFT:
            action = 1;
            switch (cursor+1) {
               case OPTION_VOLUME:
                  config.sndLevel = config.sndLevel <= 0 ? 0 : config.sndLevel - 5;
                  break;
               case OPTION_SOUND:
                  config.sndEnabled=!config.sndEnabled;
                  break;
               case OPTION_SCREENMODE:
                  config.screenMode = config.screenMode <= SCREENMODE_1X ? SCREENMODE_1X : config.screenMode - 1;
                  break;
               case OPTION_SCREENROTATE:
                  switch (config.screenRotate) {
                     case MIKIE_NO_ROTATE: config.screenRotate=MIKIE_ROTATE_R; break;
                     case MIKIE_ROTATE_L:  config.screenRotate=MIKIE_NO_ROTATE; break;
                     case MIKIE_ROTATE_R:
                     default:              config.screenRotate=MIKIE_ROTATE_L; break;
                  }
                  break;
               case OPTION_LYNXSPEED:
                  config.lynxSpeed = config.lynxSpeed <= 50 ? 50 : config.lynxSpeed - 5;
                  break;
               case OPTION_SYSTEMCLOCK:
                  switch (config.systemClock) {
                     case 300: config.systemClock=0; break;
                     case 333: config.systemClock=300; break;
                     case 366: config.systemClock=333; break;
                     case 400: config.systemClock=366; break;
                     case 433: config.systemClock=400; break;
                  }
                  if (config.systemClock<0) config.systemClock=0;
                  if (config.systemClock>433) config.systemClock=433;
                  break;
            }
            break;
         case DK_DPAD_RIGHT:
            action = 1;
            switch (cursor+1) {
               case OPTION_VOLUME:
                  config.sndLevel = config.sndLevel >= 100 ? 100 : config.sndLevel + 5;
                  break;
               case OPTION_SOUND:
                  config.sndEnabled=!config.sndEnabled;
                  break;
               case OPTION_SCREENMODE:
                  config.screenMode = config.screenMode >= SCREENMODE_STRETCH ? SCREENMODE_STRETCH : config.screenMode + 1;
                  break;
               case OPTION_SCREENROTATE:
                  switch (config.screenRotate) {
                     case MIKIE_NO_ROTATE: config.screenRotate=MIKIE_ROTATE_L; break;
                     case MIKIE_ROTATE_L:  config.screenRotate=MIKIE_ROTATE_R; break;
                     case MIKIE_ROTATE_R:
                     default:              config.screenRotate=MIKIE_NO_ROTATE; break;
                  }
                  break;
               case OPTION_LYNXSPEED:
                  config.lynxSpeed = config.lynxSpeed >= 200 ? 200 : config.lynxSpeed + 5;
                  break;
               case OPTION_SYSTEMCLOCK:
                  switch (config.systemClock) {
                     case   0: config.systemClock=300; break;
                     case 300: config.systemClock=333; break;
                     case 333: config.systemClock=366; break;
                     case 366: config.systemClock=400; break;
                     case 400: config.systemClock=433; break;
                  }
                  if (config.systemClock<0) config.systemClock=0;
                  if (config.systemClock>433) config.systemClock=433;
                  break;
            }
            break;
         case DK_BUTTON_A:
            if (cursor < ACTION_QUIT) {
               selected = cursor + 1;
               action = 1;
            }
            break;
         case DK_BUTTON_B:
            selected = ACTION_CONTINUE;
            action = 1;
            break;
         default:
            break;
         }
      }
   }

   return (ACTIONS_VIEW | selected);
}

// **********************************************************
int main(int argc, char *argv[])
{
   char pause = 0;
   int volume = 0;
   ULONG mask = 0;

   // initialize
   LynGOOInit(argc,argv);

   // loop
   while (keepRunning)
   {
      // check for system events
      int sysref = _sys_judge_event(NULL);
      if(sysref < 0) break;

      // poll controller
      control_poll();
      mask = 0;

      // check lynx keys
      if (control_pressed(config.lynxKeyUp))
         mask |= BUTTON_UP;
      else if (control_pressed(config.lynxKeyDown))
         mask |= BUTTON_DOWN;
      if (control_pressed(config.lynxKeyLeft))
         mask |= BUTTON_LEFT;
      else if (control_pressed(config.lynxKeyRight))
         mask |= BUTTON_RIGHT;
      if (control_pressed(config.lynxKeyB))
         mask |= BUTTON_B;
      if (control_pressed(config.lynxKeyA))
         mask |= BUTTON_A;
      if (control_pressed(config.lynxKeyOPT2))
         mask |= BUTTON_OPT2;
      if (control_pressed(config.lynxKeyOPT1))
         mask |= BUTTON_OPT1;
      if (control_pressed(config.lynxKeyStart))
         mask |= BUTTON_PAUSE;

      // check menu
      if ((control_pressed(DK_BUTTON_SELECT) && control_pressed(DK_BUTTON_START)) || control_pressed(DK_BUTTON_POWER)) {
         pause = 1;
         mask = 0;
      }

      // check volume up
      if (control_pressed(DK_TRIGGER_RIGHT) && control_changed(DK_TRIGGER_RIGHT)) {
         config.sndLevel = config.sndLevel >= 100 ? 100 : config.sndLevel + 5;
      }

      // check volume down
      if (control_pressed(DK_TRIGGER_LEFT) && control_changed(DK_TRIGGER_LEFT)) {
         config.sndLevel = config.sndLevel <= 0 ? 0 : config.sndLevel - 5;
      }

      // pass lynx keys
      config.lynxSystem->SetButtonData(mask);

      // set volume if needed
      if (volume!=config.sndLevel) {
         volume=config.sndLevel;
         waveout_set_volume(volume>>1);
      }

      // handle menu
      if (pause == 1)
      {
         gSystemHalt = 1;
         SoundPause();
         LynGOOMenu(ACTIONS_VIEW);
         if (keepRunning) {
            LynGOOSetup();
            pause = 0;
            gSystemHalt = 0;
            SoundUnPause();
         }
      }

      // update lynx
      if (ThrottleCheck() && !gSystemHalt) {
         config.lynxSystem->Update(config.loopCount);
      } else OSTimeDly(1);
   }

   // exit
   LynGOOExit();

   return 0;
}
