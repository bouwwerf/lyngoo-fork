#ifndef PAD_H
#define PAD_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <dingkit/control.h>

void PadInit();
void PadExit();
int PadWaitForOneKey();
void PadKeyTimer();
void PadReadKey(int *);

#endif
