#ifndef SOUND_H
#define SOUND_H

#include <stdlib.h>
#include <stdio.h>

#include "lyngoo.h"

#include "System.h"
#include "Lynxdef.h"

bool SoundInit();
void SoundExit();
void SoundPause();
void SoundUnPause();

#endif
