#include "pad.h"
#include "dingoogfx.h"

#include <dingkit/timer.h>

// *************************************************
void PadInit()
{
   control_init();
   control_lock(TIMER_RESOLUTION / 2);
}

// *************************************************
void PadExit()
{
   control_term();
}

// *************************************************
int PadWaitForOneKey()
{
   ULONG key = 0;
   for (;;)
   {
      control_poll();
      if (control_pressed(DK_DPAD_UP) && control_changed(DK_DPAD_UP))
         { key=DK_DPAD_UP; break; }
      if (control_pressed(DK_DPAD_DOWN) && control_changed(DK_DPAD_DOWN))
         { key=DK_DPAD_DOWN; break; }
      if (control_pressed(DK_DPAD_LEFT) && control_changed(DK_DPAD_LEFT))
         { key=DK_DPAD_LEFT; break; }
      if (control_pressed(DK_DPAD_RIGHT) && control_changed(DK_DPAD_RIGHT))
         { key=DK_DPAD_RIGHT; break; }
      if (control_pressed(DK_BUTTON_A) && control_changed(DK_BUTTON_A))
         { key=DK_BUTTON_A; break; }
      if (control_pressed(DK_BUTTON_B) && control_changed(DK_BUTTON_B))
         { key=DK_BUTTON_B; break; }
      if (control_pressed(DK_BUTTON_X) && control_changed(DK_BUTTON_X))
         { key=DK_BUTTON_X; break; }
      if (control_pressed(DK_BUTTON_Y) && control_changed(DK_BUTTON_Y))
         { key=DK_BUTTON_Y; break; }
      if (control_pressed(DK_TRIGGER_RIGHT) && control_changed(DK_TRIGGER_RIGHT))
         { key=DK_TRIGGER_RIGHT; break; }
      if (control_pressed(DK_TRIGGER_LEFT) && control_changed(DK_TRIGGER_LEFT))
         { key=DK_TRIGGER_LEFT; break; }
      if (control_pressed(DK_BUTTON_START) && control_changed(DK_BUTTON_START))
         { key=DK_BUTTON_START; break; }
      if (control_pressed(DK_BUTTON_SELECT) && control_changed(DK_BUTTON_SELECT))
         { key=DK_BUTTON_SELECT; break; }
   }
   for (;;)
   {
      control_poll();
      if (!control_pressed(key)) break;
   }
   return key;
}

// *************************************************
void PadKeyTimer()
{
   control_lock(TIMER_RESOLUTION / 2);
}

// *************************************************
void PadReadKey(int *key)
{
   Info("Press the desired key.", 0);
   PadKeyTimer();
   *key = PadWaitForOneKey();
   PadKeyTimer();
}
