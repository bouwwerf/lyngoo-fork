#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <malloc.h>

extern int main(int, char**);

extern char* __to_locale_ansi(wchar_t*);

/*char  __to_locale_ansi_string[256];

char* __to_locale_ansi(wchar_t* inInput) {
	uintptr_t i;
	char* tempString = (char*)inInput;
	for(i = 0; (i < 255) && (tempString[i] != '\0'); i += 2)
		__to_locale_ansi_string[i] = tempString[i];
	tempString[i] = '\0';
	return tempString;
}*/

int GameMain(wchar_t* respath) {
	char* tempAnsiPath = __to_locale_ansi(respath);
	uintptr_t tempAnsiLen;
	for(tempAnsiLen = 0; tempAnsiPath[tempAnsiLen] != '\0'; tempAnsiLen++);
	wchar_t* tempPath = malloc(tempAnsiLen + 1);
	if(tempPath == NULL)
		return EXIT_FAILURE;
	strcpy((char*)tempPath, __to_locale_ansi(respath));
	char* tempStr = (char*)tempPath;
	int tempOut = main(1, &tempStr);
	free(tempPath);
	_malloc_reclaim();
	return tempOut;
}
