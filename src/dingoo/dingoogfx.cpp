#include "dingoogfx.h"

extern LynGOOConf config;
const char *dingooKey[12] = { "UP", "DOWN", "LEFT", "RIGHT", "A", "B", "X", "Y", "START", "SELECT", "R", "L" };

display* PgDisplay = NULL;

// **********************************************************
uint16_t *PgGetVramAddr(unsigned long x, unsigned long y)
{
   return (uint16_t *)PgDisplay->buffer + (y * PgDisplay->stride) + x;
}

// **********************************************************
void PgPutChar(unsigned long x, unsigned long y, unsigned long color, unsigned long bgcolor, unsigned char ch, char drawfg, char drawbg, char mag)
{
   uint16_t *vptr0;
   uint16_t *vptr;
   const unsigned char *cfont;
   unsigned long cx, cy;
   unsigned long b;
   char mx, my;

   cfont = font + ch * 8;
   vptr0 = PgGetVramAddr(x, y);
   for (cy = 0; cy < 8; cy++)
   {
      for (my = 0; my < mag; my++)
      {
         vptr = vptr0;
         b = 0x80;
         for (cx = 0; cx < 8; cx++)
         {
            for (mx = 0; mx < mag; mx++)
            {
               if ((*cfont & b) != 0)
                  if (drawfg)
                     *vptr = color;
                  else if (drawbg)
                     *vptr = bgcolor;
               vptr++;
            }
            b = b >> 1;
         }
         vptr0 += PgDisplay->stride;
      }
      cfont++;
   }
}

// **********************************************************
void PgPrint(unsigned long x, unsigned long y, unsigned long color, const char *str, int bg)
{
   while (*str != 0 && x < CMAX_X && y < CMAX_Y)
   {
      PgPutChar(x * 8, y * 8, color, 0, *str, 1, bg, 1);
      str++;
      x++;
      if (x >= CMAX_X)
      {
         x = 0;
         y++;
      }
   }
}

// **********************************************************
void PgInit()
{
   PgDisplay = display_create(DINGOO_SCREEN_WIDTH, DINGOO_SCREEN_HEIGHT, DINGOO_LINE_SIZE, (DISPLAY_FORMAT_RGB565 | DISPLAY_STRETCH), NULL, NULL);
}

// **********************************************************
void PgExit()
{
   display_delete(PgDisplay);
}

// **********************************************************
void PgSetVisible(int width, int height)
{
   PgDisplay->width=width;
   PgDisplay->stride=width;
   PgDisplay->height=height;
}

// **********************************************************
void PgScreenFlip()
{
   display_flip(PgDisplay);
}

// **********************************************************
void PgBoxGreyTransparent(int x1, int y1, int x2, int y2)
{
   uint16_t *vptr0;

   vptr0 = PgGetVramAddr(0, 0);

   for (int y = y1; y < y2; y++)
      for (int x = x1; x < x2; x++)
         vptr0[x + y * PgDisplay->stride] += DINGOO_RGB(30, 30, 30);
}

// **********************************************************
void ClearDoubleBuffer()
{
   memset(PgGetVramAddr(0, 0), 0, PgDisplay->height * PgDisplay->stride * sizeof(uint16_t));
}

// **********************************************************
void DrawHorizontalLine(int x1, int x2, int y, int color)
{
   uint16_t *vptr0 = PgGetVramAddr(0, 0);

   for (int x = x1; x <= x2; x++)
      vptr0[x + y * PgDisplay->stride] = color;
}

// **********************************************************
void DrawVerticalLine(int y1, int y2, int x, int color)
{
   uint16_t *vptr0 = PgGetVramAddr(0, 0);

   for (int y = y1; y <= y2; y++)
      vptr0[x + y * PgDisplay->stride] = color;
}

// **********************************************************
void FillRect(int x1, int x2, int y1, int y2, int color)
{
   uint16_t *vptr0 = PgGetVramAddr(0, 0);

   for (int y = y1; y <= y2; y++)
      for (int x = x1; x <= x2; x++)
         vptr0[x + y * PgDisplay->stride] = color;
}

// **********************************************************
void DrawCadre(int x1, int x2, int y1, int y2)
{
   DrawHorizontalLine(x1, x2, y1, LIGHT_GREY);
   DrawHorizontalLine(x1 + 1, x2 - 1, y1 + 1, GREY);
   DrawHorizontalLine(x1 + 2, x2 - 2, y1 + 2, LIGHT_GREY);

   DrawHorizontalLine(x1 + 3, x2 - 3, y1 + 10, LIGHT_GREY);
   DrawHorizontalLine(x1 + 3, x2 - 3, y1 + 11, GREY);
   DrawHorizontalLine(x1 + 3, x2 - 3, y1 + 12, LIGHT_GREY);

   DrawHorizontalLine(x1 + 2, x2 - 2, y2 - 2, LIGHT_GREY);
   DrawHorizontalLine(x1 + 1, x2 - 1, y2 - 1, GREY);
   DrawHorizontalLine(x1, x2, y2, LIGHT_GREY);

   DrawVerticalLine(y1, y2, x1, LIGHT_GREY);
   DrawVerticalLine(y1 + 1, y2 - 1, x1 + 1, GREY);
   DrawVerticalLine(y1 + 2, y2 - 2, x1 + 2, LIGHT_GREY);

   DrawVerticalLine(y1 + 2, y2 - 2, x2 - 2, LIGHT_GREY);
   DrawVerticalLine(y1 + 1, y2 - 1, x2 - 1, GREY);
   DrawVerticalLine(y1, y2, x2, LIGHT_GREY);
}

// **********************************************************
void DrawBox(int x1, int x2, int y1, int y2)
{
   DrawCadre(x1, x2, y1, y2);

   FillRect(x1 + 3, x2 - 3, y1 + 3, y1 + 9, LIGHT_BLUE);
   FillRect(x1 + 3, x2 - 3, y1 + 13, y2 - 3, WHITE);
}

// **********************************************************
int InfoBox(const char *titre, const char *message, bool waitForKey)
{
   int len = strlen(message);
   int x, y, xbase;

   DrawBox(64, 255, 69, 160);
   PgPrint(9, 9, RED, titre, 0);
   x = xbase = 9;
   y = 11;
   for (int i = 0; i < len; i++)
   {
      PgPutChar(x * 8, y * 8, BLACK, 0, message[i], 1, 0, 1);

      x++;

      if (x * 8 >= 255)
      {
         x = xbase;
         y++;
      }

   }

   if (waitForKey) PgPrint(9, 18, BLUE, "A to continue", 0);

   PgScreenFlip();

   if (waitForKey) return PadWaitForOneKey();

   return 0;
}

// **********************************************************
void Error(const char *message, bool key)
{
   InfoBox("Error", message, key);
}

// **********************************************************
void Info(const char *message, bool key)
{
   InfoBox("Info", message, key);
}

// **********************************************************
int Question(const char *message)
{
   return InfoBox("Please confirm", message, true);
}
