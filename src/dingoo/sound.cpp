#include "sound.h"

#define  SOUND_TASK_START_PRIO 5                         // Must be unique!
#define  SOUND_TASK_STK_SIZE   1024                      // Size of each task's stacks (# of WORDs)

OS_STK   SoundTaskStartStk[SOUND_TASK_STK_SIZE];

volatile bool sound_thread_active = false;
static bool sound_active = false;
waveout_inst *snd_handle;
short soundBuffer[HANDY_AUDIO_BUFFER_SIZE];
volatile ULONG soundPtr=0,soundLen=0;

// *************************************************
static void SoundThread(void *none)
{
   sound_thread_active=true;
   do
   {
       if (sound_active && gAudioBufferPointer>512) {
          ULONG soundLen=gAudioBufferPointer;
          for (ULONG i = 0; i < soundLen; i++) {
             soundBuffer[i]=(gAudioBuffer[i]<<8)-32768;
          }
          gAudioBufferPointer = 0;
          waveout_write(snd_handle, (char*)soundBuffer, soundLen*sizeof(soundBuffer[0]));
       } else OSTimeDly(1);
   }
   while (sound_thread_active);
   sound_thread_active=true;
   OSTaskDel(SOUND_TASK_START_PRIO);
}

// *************************************************
bool SoundInit()
{
   memset(soundBuffer, 0, HANDY_AUDIO_BUFFER_SIZE*sizeof(soundBuffer[0]));
   waveout_args tempArgs = { HANDY_AUDIO_SAMPLE_FREQ, AFMT_S16_LE, 1, 25 };
   snd_handle = waveout_open(&tempArgs);
   sound_thread_active=false;
   SoundUnPause();
   return (OSTaskCreate(SoundThread, NULL, &SoundTaskStartStk[SOUND_TASK_STK_SIZE - 1], SOUND_TASK_START_PRIO) == 0);
}

// *************************************************
void SoundExit()
{
   SoundPause();
   if (sound_thread_active) {
      sound_thread_active=false;
      while (!sound_thread_active);  // to avoid pb with waveout_close -> can hang ??
   }
   waveout_close(snd_handle);
}

// *************************************************
void SoundPause()
{
   sound_active=false;
}

// *************************************************
void SoundUnPause()
{
   sound_active=true;
}
