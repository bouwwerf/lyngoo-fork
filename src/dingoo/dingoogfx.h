#ifndef DINGOOGFX_H
#define DINGOOGFX_H

#include <stdlib.h>
#include <stdio.h>

#include <dingkit/display.h>
#include <dingkit/control.h>
#include <dingkit/timer.h>

#include "pad.h"
#include "font.h"
#include "lyngoo.h"

#define DINGOO_RGB(r,g,b)	(((r>>3)<<11)|((g>>2)<<5)|(b>>3))
#define DINGOO_SCREEN_WIDTH 	320
#define DINGOO_SCREEN_HEIGHT 	240
//#define DINGOO_SCREEN_WIDTH 	160
//#define DINGOO_SCREEN_HEIGHT 	120
#define	DINGOO_PIXEL_FORMAT 	DISPLAY_FORMAT_RGB565
#define	DINGOO_LINE_SIZE 	DINGOO_SCREEN_WIDTH
#define CMAX_X 			DINGOO_SCREEN_WIDTH/8
#define CMAX_Y 			DINGOO_SCREEN_HEIGHT/8

#define LIGHT_GREY		DINGOO_RGB(170,170,170)
#define GREY			DINGOO_RGB(100,100,100)
#define BLACK			DINGOO_RGB(0,0,0)
#define WHITE			DINGOO_RGB(255,255,255)
#define RED			DINGOO_RGB(255,0,0)
#define GREEN			DINGOO_RGB(0,255,0)
#define BLUE			DINGOO_RGB(0,0,255)
#define YELLOW			DINGOO_RGB(255,255,0)
#define LIGHT_BLUE		DINGOO_RGB(167,193,216)

int GetKeyIndex(int key);
uint16_t *PgGetVramAddr(unsigned long, unsigned long);
void PgInit();
void PgExit();
void PgSetVisible(int width, int height);
void PgScreenFlip();
void PgPutChar(unsigned long, unsigned long, unsigned long, unsigned long, unsigned char, char, char, char);
void PgPrint(unsigned long, unsigned long, unsigned long, const char *, int);
void PgBoxGreyTransparent(int, int, int, int);
void DrawHorizontalLine(int, int, int, int);
void DrawVerticalLine(int, int, int, int);
void FillRect(int, int, int, int, int);
void DrawBox(int, int, int, int);
void DrawCadre(int, int, int, int);
void ClearDoubleBuffer();
int InfoBox(const char *, const char *, bool);
void Error(const char *, bool);
void Info(const char *, bool);
int Question(const char *);
#endif
