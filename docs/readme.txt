LynGOO v1.4
===================================================================================

LynGOO is a Dingoo A320 port of Keith Wilkins Atari Lynx emulator Handy.
This port is partly based on a PSP port of Handy, Plynx (http://plynx.dcemu.co.uk).

Do not ask me for ROMs. I don't have them. Google is your friend.


Important note regarding save states
===================================================================================

The format for save state files has changed in v1.1. This means that save states
from previous versions of LynGOO can no longer be loaded. This is an unfortunate
but neccessary change.

On the bright side: save states should now be compatible with the original Handy
emulator and its ports.


Usage
===================================================================================

Follow these steps to enjoy playing Atari Lynx games on your Dingoo:

1) Google for the 'lynxboot.img' file and download it.
   Due to licensing restrictions I cannot bundle it with this emulator.

2) Install LynGOO.SIM and lynxboot.img in the GAME folder of your Dingoo.

3) Install your *.LNX files on your Dingoo (for example in GAME/LNX).

4) Select one of the LNX files from the "Interesting game" menu item.

Press SELECT+START to bring up the emulator menu. The volume can be changed
in-game by pressing the left and right shoulder buttons.


Changelog
===================================================================================

v1.4 (9 Dec 2021)
  - Improvements to the superclip renderer by trioan. Affected games include Warbirds.
  - Removed Handy emulator detection so Zaku can load properly. Found by zakuahem.

v1.3 (14 Sep 2016)
  - Fixed Everon detection bug.
	Reported and fixed by Alex Thissen (LX.NET) and Björn Spruck (sage) on Atariage.com. Affected games include Robotron 2084 and Pong1K demo.
  - Fixed Hardware bug emulation of SCB data ending exactly at the last bit of shiftregister.
	Reported by Björn Spruck (sage) and fixed by Alex Thissen (LX.NET) on AtariAge.com. Affected games include Joust and Wuerfel demo.

v1.2 (21 Nov 2009)
  - Handle system events (exit on USB plugin etc).
  - Performance optimizations.
  - Added overclocking controls.
  - Added lynx speed control.

v1.1 (15 Nov 2009)
  - Added screen rotation controls.
  - Added sound on/off control.
  - Added CRC32 checks to save/load state.
  - CPU Speed set to 400MHz.
  - Frameskip disabled; not effective.

v1.0 (11 Nov 2009)
  - Initial release.


Credits
===================================================================================

Keith Wilkins - Creator of Handy
Matthijs van der Klip (Spiller) - Creator of LynGOO
Ben Brewer (flatmush) and Alekmaul - Dingoo code
Alex Thissen (LX.NET), Björn Spruck (sage), trioan, zakuahem - Handy improvements
